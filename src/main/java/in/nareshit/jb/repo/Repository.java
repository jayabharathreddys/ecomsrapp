package in.nareshit.jb.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.jb.entity.Coupon;

public interface Repository extends JpaRepository<Coupon, Long>{

}
